/**
 * Created by peter on 2016. 05. 07..
 */

describe('RomanNumberConverter tests', function () {
    let romanNumberConverterService: com.babcsany.tdd.converter.roman.IRomanNumberConverterService;

    beforeEach(function () {
        angular.mock.module('com.babcsany.tdd.converter.roman');
    });

    beforeEach(function () {
        inject(function (RomanNumberConverterService) {
            romanNumberConverterService = RomanNumberConverterService;
        });
    });
    
    describe('convert', function () {
        it('should convert numbers to roman numbers correctly', function () {
            expect(romanNumberConverterService.convert(1)).toEqual('I');
            expect(romanNumberConverterService.convert(2)).toEqual('II');
            expect(romanNumberConverterService.convert(3)).toEqual('III');
            expect(romanNumberConverterService.convert(5)).toEqual('V');
            expect(romanNumberConverterService.convert(8)).toEqual('VIII');
            expect(romanNumberConverterService.convert(10)).toEqual('X');
            expect(romanNumberConverterService.convert(18)).toEqual('XVIII');
            expect(romanNumberConverterService.convert(20)).toEqual('XX');
            expect(romanNumberConverterService.convert(4)).toEqual('IV');
            expect(romanNumberConverterService.convert(9)).toEqual('IX');
            expect(romanNumberConverterService.convert(44)).toEqual('XLIV');
            expect(romanNumberConverterService.convert(50)).toEqual('L');
            expect(romanNumberConverterService.convert(399)).toEqual('CCCXCIX');
            expect(romanNumberConverterService.convert(1449)).toEqual('MCDXLIX');
            expect(romanNumberConverterService.convert(3994)).toEqual('MMMCMXCIV');
            expect(romanNumberConverterService.convert(3999)).toEqual('MMMCMXCIX');
        });
    });
});
