/**
 * Created by peter on 2016. 05. 07..
 */

module.exports = function (w) {
    return {
        files: [
            { "pattern" : "bower_components/jquery/dist/jquery.js", "instrument": false },
            { "pattern" : "bower_components/angular/angular.js", "instrument": false },
            { "pattern" : "bower_components/angular-mocks/angular-mocks.js", "instrument": false },
            'src/*.ts'
        ],

        tests: [
            'test/*.spec.ts'
        ]
    };
};
