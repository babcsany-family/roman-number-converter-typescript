/**
 * Created by peter on 2016. 05. 07..
 */

namespace com.babcsany.tdd.converter.roman {

    export interface IRomanNumberConverterService {
        convert(num: number);
    }

    enum RomanDigit {
        M = 1000,
        CM = 900, D = 500, CD = 400, C = 100,
        XC = 90, L = 50, XL = 40, X = 10,
        IX = 9, V = 5, IV = 4, I = 1
    }
    
    class RomanNumberConverterService implements IRomanNumberConverterService {
        public convert(num: number) {
            let result: string = '';

            while (num > 0) {
                for (let digit in RomanDigit) {
                    if (num >= <any>RomanDigit[digit]) {
                        num -= <any>RomanDigit[digit];
                        result += digit;
                        break;
                    }
                }
            }

            return result;
        }
    }
    
    angular.module('com.babcsany.tdd.converter.roman', []).service('RomanNumberConverterService', RomanNumberConverterService);
}
